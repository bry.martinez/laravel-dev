# laravel-dev
A lightweight, automated development environment for Laravel.

# Requirements
Docker
PHP 7 or higher

# Instructions
Run ./bin/start.sh
