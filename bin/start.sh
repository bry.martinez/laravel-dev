#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/shared_functions.sh
h1 'Installing Composer'
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer
export PATH=$PATH:$HOME/.composer/vendor/bin
h1 'Composer Installed'
h1 'Installing PHP dependencies'
sudo apt-get install php-gd php-xml php-mbstring php-zip
h1 'PHP Dependencies Installed'
h1 'Getting Laravel Installer'
composer install -d ${DIR}/../src
composer global require "laravel/installer"
h1 'Laravel Installer Completed'
h1 'Starting Docker Configuration'
docker-compose up -d
cp ${DIR}/../src/.env.example ${DIR}/../src/.env
sudo chmod -R 777 ${DIR}/../src/storage
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan optimize
h1 'Docker Configuration Started! Happy coding!'

#!/bin/bash
if which xdg-open > /dev/null
then
  xdg-open http://localhost:8080 &
elif which gnome-open > /dev/null
then
  gnome-open http://localhost:8080 &
fi
